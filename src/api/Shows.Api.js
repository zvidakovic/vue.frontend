import apiClient from "./ApiClient";

const getShowsByQuery = (query, page = 1, itemsPerPage = 3) => {
    return apiClient.get("/", {
        params: {
            q: query,
            page: page,
            itemsPerPage: itemsPerPage
        }
    }).then((response) => {
        if (response.status === 200) {
            return response.data;
        }
        return [];
    });
}

export {
    getShowsByQuery,
}

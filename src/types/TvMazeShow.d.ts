export class TvMazeShow {
  score: number
  show: {
    id: number,
    "url": string,
    "name": string,
    "type": string,
    "language": string,
    "genres": Array<string>,
    "status": string,
    "runtime": number,
    "premiered": string,
    "officialSite": string,
    "schedule": {
      "time": string,
      "days": Array<string>
    },
    "rating": {
      "average": number
    },
    "weight": number,
    "network": {
      "id": number,
      "name": string,
      "country": {
        "name": string,
        "code": string,
        "timezone": string
      }
    },
    "webChannel": string,
    "externals": {
      "tvrage": number,
      "thetvdb": number,
      "imdb": string
    },
    "image": {
      "medium": string,
      "original": string
    },
    "summary": string,
    "updated": number,
  }
}
